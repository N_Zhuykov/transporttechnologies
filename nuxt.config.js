export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TransportTechnologies',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ],
    script:[
      //{src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js', type: "text/javascript"}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/styles/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/auth-next'
  ],

  axios: {
    proxy: true
  },

  proxy: {
    // '/api/': {target: 'localhost:5000', pathRewrite: {'^/api/': ''}},
    //'/api/v2':  'http://localhost:8080/',
    '/api/v1': { target: 'http://localhost:8080/', pathRewrite: {'^/api/v1': ''} },
    '/api/v2': { target: 'http://localhost:8080/api', pathRewrite: {'^/api/v2': ''} },
    '/api/v3': { target: 'http://azs.mcofe.beget.tech/azs', pathRewrite: {'^/api/v3': ''} }
  },

  auth: {
    localStorage: false,
    strategies: {
      local: {
        token: {
          property: 'token',
          global: true,
          type: 'Bearer'
        },
        user:{
          property: 'user'
        },
        endpoints: {
          login: { url: '/api/v2/auth/login', method: 'post'},
          // login: { url: '/api/v3/authorization_admin', method: 'get'},
          logout: false,
          user: { url: '/api/v2/auth/user', method: 'get'}
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
}
