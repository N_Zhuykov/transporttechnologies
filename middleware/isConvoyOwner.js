export default function ({ $auth, redirect }) {
  if (!$auth.hasScope('convoyOwner')){
    return redirect('/error')
  }
}
