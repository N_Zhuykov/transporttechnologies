export default () => ({
  user: {
    "id": 1,
    "phone": null,
    "balance": "100.00",
    "name": "Олег",
    "surname": "Олегович",
    "email": "admin@admin.ru",
    "login": "login",
    "place_work": "Рублёвка",
    "azs_id": null,
    "autoconvoy_id": null,
    "roles": 1,
    "creator": null,
    "public": 0,
    "created_at": "2021-08-19T10:49:55.000000Z"
  },

  cards: []
})
