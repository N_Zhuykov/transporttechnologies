export default {
  async AddNewCard(context, payload) {
    context.commit('addNewCard', payload)
  },

  async GetUser(context) {
    try {
      this.$axios.get('/api/v3/user')
        .then((res) => {
          context.commit('setUser', res.data.user)
        })
    } catch (error) {
    }
  }

}
