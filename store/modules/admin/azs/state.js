export default () => ({
  // cards: [{
  //   name: "Автоколонна «Европа» 1",
  //   // lastTransaction: new Date("2021-08-12T20:30:00.417Z"),
  //   lastTransaction: "12/08/2021 18:23",
  //   latitude: 2121.21,
  //   longitude: 3232.23,
  //   balance: 12345,
  //   people: 1000,
  //   options: [
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //   ]
  // },{
  //   name: "Автоколонна «Европа» 2",
  //   // lastTransaction: new Date("2021-08-12T20:30:00.417Z"),
  //   lastTransaction: "12/08/2021 18:23",
  //   latitude: 2121.21,
  //   longitude: 3232.23,
  //   balance: 12345,
  //   people: 1000,
  //   options: [
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //   ]
  // },{
  //   name: "Автоколонна «Европа» 3",
  //   // lastTransaction: new Date("2021-08-12T20:30:00.417Z"),
  //   lastTransaction: "12/08/2021 18:23",
  //   latitude: 2121.21,
  //   longitude: 3232.23,
  //   balance: 12345,
  //   people: 1000,
  //   buttons: [
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //     {name: "92 евро", price: 21.21},
  //   ]
  // },]
  cards: [
    {
      "name": "Азс «Европа» 3",
      "lat": "-44.5555552",
      "lng": "37.55555552",
      "azs_id": 11,
      "user_id": 48,
      "options": [
        {
          "id_options": 4,
          "name": "92 евро",
          "price": "429.55",
          "discounted_price": "0.00"
        },
        {
          "id_options": 5,
          "name": "92 евро",
          "price": "529.55",
          "discounted_price": "0.00"
        }
      ]
    },
    {
      "name": "Азс «Европа»",
      "lat": "-44.5555552",
      "lng": "37.55555552",
      "azs_id": 13,
      "user_id": 48,
      "options": []
    }
  ]

})
